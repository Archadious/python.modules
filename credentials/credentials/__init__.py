# Time-stamp: <2020-02-04 09:55:26 daniel>
# -*- mode: python; -*-


##
## Required modules
#------------------------------------------------------------------------------
from pversion import PVersion  # Defines quick access to Python version
from json import dumps, loads  # JSON encoding support
from os import environ  # System ENVIRONMENT variable access
import gnupg  # gnupg support


##
## Determine default path to gnupg directory if not supplied
##-----------------------------------------------------------------------------
class GPGPath( object ) :

  ## Constructor
  def __init__( self, gpghome ) :
    if PVersion.Three : super().__init__()
    else : super( GPGPath, self ).__init__()
    self.path = environ[ 'HOME' ] + '/.gnupg' if gpghome == None else gpghome


##
## Return default credential file name if not supplied
##-----------------------------------------------------------------------------
class GPGFile( object ) :

  # Default credital file name
  Name = 'credential_file.gpg'

  ## Constructor
  def __init__( self, gpgfile ) :
    if PVersion.Three : super().__init__()
    else : super( GPGFile, self ).__init__()
    self.name = GPGFile.Name if gpgfile == None else gpgfile


##
## Encrypt / Decrypt the user and password
##-----------------------------------------------------------------------------
class Credentials( object ) :

  ## Constructor
  def __init__( self, home = None ) :
    if PVersion.Three : super().__init__()
    else : super( Credentials, self ).__init__()
    self.gpg = gnupg.GPG( gnupghome = GPGPath( home ).path )

  ## Set the data after decryption
  def set_data( self ) :
    data = loads( str( self.status ) )
    self.user = data[ 'user' ]
    self.password = data[ 'password' ]

  ## Decrypt the named credital file and set the data
  def decrypt( self, cfile = None ) :
    with open( GPGFile( cfile ).name, 'rb' ) as F :
      self.status = self.gpg.decrypt( F.read() )
    self.set_data()

  ## Encrypt the 'user' and 'password' and store in a credential file
  def encrypt( self, user, password, email, cfile = None ) :
    dstring = dumps( { 'user': user, 'password': password } )
    status = self.gpg.encrypt( dstring, email )
    with open( GPGFile( cfile ).name, 'w' ) as F :
      F.write( str( status ) )
