# Time-stamp: <2020-02-04 09:52:15 daniel>
# -*- mode: python; -*-

# Use Example:
#
#    from credentials import Credentials
#
#

## Required module
from setuptools import setup

## The main setup function - Called by 'pip install'
setup( name = 'credentials',
       version = '0.1',
       description = 'Encrypt / Decrypt user and password',
       url = '',
       author = 'Archadious',
       author_email = '',
       license = 'MIT',
       packages = [ 'credentials' ],
       zip_safe = True
)  # end setup
